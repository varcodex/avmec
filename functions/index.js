'use strict';

const functions = require('firebase-functions')
const admin=require('firebase-admin');
const nodemailer =require('nodemailer');

const gmailEmail = functions.config().gmail.login;
const gmailPassword = functions.config().gmail.pass;

admin.initializeApp()

exports.sendEmailNotification=functions.firestore.document('reservation/{docId}')
.onCreate((snap,ctx)=>{
    const data=snap.data();
     var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
          user: gmailEmail,
          pass: gmailPassword
      }
    });

    const mailOptions = {
      from: gmailEmail,
      to: `${data.email}`,
      subject: `THANK YOU FOR CHOOSING AVMEC`,
      text: `Dear ${data.firstName} ${data.lastName}, Thank you for your booking we will contact you as soon as possible!`,
      html:`Dear ${data.firstName} ${data.lastName}, Thank you for your booking we will contact you as soon as possible!`,
    };

    transporter.sendMail(mailOptions)
    .then(res => {
        console.log('Succesfully Sent');
        return null;
    })
    .catch(err => {
        console.log(err);
        return null;
    });
});
