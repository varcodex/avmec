export interface Reservation {
  id: string;
  firstName: string;
  lastName: string;
  mobileNumber: string;
  email: string;
  areaCode: string;
  phoneNumber: string;
  endDate: string;
  startDate: string;
  typeOfevent: string;
  typeOfhall: string;
  numberOfguest: string;
  requestDate: string;
  requestTime: string;
  specialRequest: string;
  status: string;
  createdAt: string;
  updatedAt: string;
}
