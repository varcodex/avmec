import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Reservation } from 'src/app/models/reservation/reservation';
import { formatDate } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private firestore: AngularFirestore) { }

  async bookReservation(reservation: Partial<Reservation>){
    return await this.firestore.collection('reservation').add(reservation);
  }

  getBooking(){
    return this.firestore.collection('reservation').snapshotChanges();
  }

  getBookingById(id) {
    const data = this.firestore.doc('reservation/' + id).valueChanges();
    return data;
  }

  updateBooking(id, reservation: Partial<Reservation>) {
    return this.firestore.collection('reservation').doc(id).set(reservation, {merge: true});
  }

  getBookingDates(){
    let date = formatDate(new Date(), 'yyyy-MM-dd', 'en-PH', '+08:00')
    return this.firestore.collection('reservation', ref => ref.where('requestDate', '>=', `${date}`).where('status', '==', 'approved')).snapshotChanges();
  }

}
