import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ReservationService } from '../../../../services/reservation/reservation.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { BookingDetailsComponent } from '../booking-details/booking-details.component';
@Component({
  selector: 'app-booking-list',
  templateUrl: './booking-list.component.html',
  styleUrls: ['./booking-list.component.css']
})
export class BookingListComponent implements OnInit {
  listData: MatTableDataSource<any>;
  displayedColumns: string [] = ['firstName', 'lastName', 'mobileNumber',
                                'email', 'typeOfevent', 'typeOfhall', 'requestDate',
                                'status', 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;
  constructor(public service: ReservationService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.service.getBooking().subscribe(data => {
      let array = data.map(e => {
          return {
              id: e.payload.doc.id,
              ...e.payload.doc.data() as {}
          };
      });
      this.listData = new MatTableDataSource(array);
      this.listData.sort = this.sort;
      this.listData.paginator = this.paginator;
    });
  }

  onSearchClear(){
    this.searchKey = '';
    this.applyFilter();
  }
  applyFilter(){
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  doView(row){
    if(row.id){
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "800px";
        dialogConfig.height = "500px";
        dialogConfig.data = {id: row.id};
        this.dialog.open(BookingDetailsComponent, dialogConfig);
    }
  }

}
