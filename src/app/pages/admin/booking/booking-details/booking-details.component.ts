import { Component, OnInit, Inject, NgZone, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ReservationService } from '../../../../services/reservation/reservation.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { formatDate } from '@angular/common';
import { Reservation } from '../../../../models/reservation/reservation';
import {take} from 'rxjs/operators';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.css']
})
export class BookingDetailsComponent implements OnInit {
  bookingId: any;
  reservation: Reservation;
  requestDatectrl = new FormControl();
  isClose: boolean;
  typeOfhall: any;
  typeOfevent: any;
  status: string;
  unavailableDates: Date[] = [];
  mindate =  new Date();
  tempRequestDate: any;
  constructor(private reservationService: ReservationService,
              private _ngZone: NgZone,
              private dialogRef: MatDialogRef<BookingDetailsComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
                if(this.data.id){
                  this.bookingId = this.data.id;
                }
  }

  bookingForm = new FormGroup({
    firstName: new FormControl(),
    lastName: new FormControl(),
    mobileNumber: new FormControl(),
    email: new FormControl(),
    numberOfguest: new FormControl(),
    requestDate: new FormControl(),
    requestTime: new FormControl(),
    specialRequest: new FormControl(),
  })
  @ViewChild('autosize') autosize: CdkTextareaAutosize;

    triggerResize() {
        // Wait for changes to be applied, then trigger textarea resize.
        this._ngZone.onStable.pipe(take(1))
  }
  async ngOnInit(): Promise<void> {
    this.reservationService.getBookingById(this.bookingId).subscribe(
      (result: Reservation) => {
          if (result) {
              this.reservation = result;
              this.bookingForm.patchValue({
                firstName: this.reservation.firstName,
                lastName: this.reservation.lastName,
                mobileNumber: this.reservation.mobileNumber,
                email: this.reservation.email,
                numberOfguest: this.reservation.numberOfguest,
                specialRequest: this.reservation.specialRequest,
                requestTime: this.reservation.requestTime,
              });
              this.requestDate.setValue(this.reservation.requestDate);
              this.typeOfhall = this.reservation.typeOfhall;
              this.typeOfevent = this.reservation.typeOfevent;
              this.status = this.reservation.status;
              this.tempRequestDate = this.reservation.requestDate
          }
      });
      this.reservationService.getBookingDates().subscribe(data => {
        let array = data.map(e => {
            return {
                id: e.payload.doc.id,
                ...e.payload.doc.data() as {}
            };
        });
        for(let data of array) {
          let date = formatDate(this.tempRequestDate, 'yyyy-MM-dd', 'en-PH', '+08:00')
          if(data['requestDate'] != date){
            this.unavailableDates.push(data['requestDate']);
          }
        }
      });
  }

  doClose(){
    this.dialogRef.close();
  }

  myFilter = (d: Date): boolean => {
    let data = formatDate(d, 'yyyy-MM-dd', 'en-PH', '+08:00')
    return !this.unavailableDates.find(x=>
      formatDate(x, 'yyyy-MM-dd', 'en-PH', '+08:00')==data);
  }
  async doSubmit(){
    await this.reservationService.updateBooking(this.bookingId, {
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      mobileNumber: this.mobileNumber.value,
      email: this.email.value,
      numberOfguest: this.numberOfguest.value,
      specialRequest: this.specialRequest.value,
      requestDate: formatDate(this.requestDate.value, 'yyyy-MM-dd', 'en-PH', '+08:00'),
      requestTime: this.requestTime.value,
      typeOfhall: this.typeOfhall,
      typeOfevent: this.typeOfevent,
      status: this.status,
      updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
    }).then(
      () => {
          this.dialogRef.close();
      }
  );
  }

  comboChange(event) {
    this.isClose = false;
    if(!event) {
    this.isClose = true;
    }
  }

  get requestDate() {return this.bookingForm.get('requestDate');}
  get requestTime() {return this.bookingForm.get('requestTime');}
  get email() {return this.bookingForm.get('email');}
  get firstName() {return this.bookingForm.get('firstName');}
  get mobileNumber() {return this.bookingForm.get('mobileNumber');}
  get lastName() {return this.bookingForm.get('lastName');}
  get numberOfguest() {return this.bookingForm.get('numberOfguest');}
  get specialRequest() {return this.bookingForm.get('specialRequest');}

}
