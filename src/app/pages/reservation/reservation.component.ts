import { Component, OnInit, Inject, NgZone, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { formatDate } from '@angular/common';
import { ReservationService } from '../../services/reservation/reservation.service';
import {take} from 'rxjs/operators';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';


@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {
  reservationForm: FormGroup;
  submitted = false;
  mindate =  new Date();
  unavailableDates: Date[] = [];
  constructor(public reservationService: ReservationService,
    private _ngZone: NgZone,
    private formBuilder: FormBuilder) {

   }
   @ViewChild('autosize') autosize: CdkTextareaAutosize;

   triggerResize() {
       // Wait for changes to be applied, then trigger textarea resize.
       this._ngZone.onStable.pipe(take(1))
  }
  ngOnInit() {
    this.reservationForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      mobileNumber: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      numberOfguest: ['', Validators.required],
      typeOfevent: ['', Validators.required],
      typeOfhall: ['', Validators.required],
      requestDate: ['', Validators.required],
      requestTime: ['', Validators.required],
      specialRequest: ['']
    });
    this.reservationService.getBookingDates().subscribe(data => {
      let array = data.map(e => {
          return {
              id: e.payload.doc.id,
              ...e.payload.doc.data() as {}
          };
      });
      for(let data of array) {
        this.unavailableDates.push(data['requestDate']);
      }
    });
  }

  myFilter = (d: Date): boolean => {
    let data = formatDate(d, 'yyyy-MM-dd', 'en-PH', '+08:00')
    return !this.unavailableDates.find(x=>
      formatDate(x, 'yyyy-MM-dd', 'en-PH', '+08:00')==data);
  }

  async onSubmit(){
    this.submitted = true;
    if (this.reservationForm.invalid) {
      return;
    }

    try {
      await this.reservationService.bookReservation({
          firstName: this.firstName.value,
          lastName: this.lastName.value,
          mobileNumber: this.mobileNumber.value,
          email: this.email.value,
          numberOfguest: this.numberOfguest.value,
          typeOfevent: this.typeOfevent.value,
          typeOfhall: this.typeOfhall.value,
          requestDate: formatDate(this.requestDate.value, 'yyyy-MM-dd', 'en-PH', '+08:00'),
          requestTime: this.requestTime.value,
          specialRequest: this.specialRequest.value,
          status: 'pending',
          createdAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
          updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        });
        alert(`Dear ${this.firstName.value} ${this.lastName.value}, Thank you for your booking we will contact you as soon as possible!`);
    } catch (error) {
        window.alert(error);
    }

  }

  // convenience getter for easy access to form fields
  get f() { return this.reservationForm.controls; }
  get firstName() { return this.reservationForm.get('firstName'); }
  get lastName() { return this.reservationForm.get('lastName'); }
  get mobileNumber() { return this.reservationForm.get('mobileNumber'); }
  get email() { return this.reservationForm.get('email'); }
  get typeOfevent() { return this.reservationForm.get('typeOfevent'); }
  get typeOfhall() { return this.reservationForm.get('typeOfhall'); }
  get numberOfguest() { return this.reservationForm.get('numberOfguest'); }
  get requestDate() { return this.reservationForm.get('requestDate'); }
  get requestTime() { return this.reservationForm.get('requestTime'); }
  get specialRequest() { return this.reservationForm.get('specialRequest'); }
}
