// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyA8L-NMTRz9j1q_BO11Eac2qQTWOhuozRs",
    authDomain: "avmec-devs.firebaseapp.com",
    databaseURL: "https://avmec-devs.firebaseio.com",
    projectId: "avmec-devs",
    storageBucket: "avmec-devs.appspot.com",
    messagingSenderId: "925648776293",
    appId: "1:925648776293:web:9e6c0cb3617be2523daa8c",
    measurementId: "G-B3QKB0FDP1"
  },
  larkFunctions: {
    url: ''
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
